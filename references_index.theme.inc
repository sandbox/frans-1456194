<?php

/**
 * Implements HOOK_preprocess_THEME().
 *
 * Preprocess theme function and fetch all parents.
 */
function references_index_preprocess_references_index_breadcrumbs(&$variables, $hook) {
  global $language;
  $variables['parents'] = array();
  $entity = entity_load($variables['entity_type'], array($variables['entity_id']));
  $entity = $entity[$variables['entity_id']];
  $langcode = (isset($entity->{$variables['field_name']}[$language->language]) ? $language->language : LANGUAGE_NONE);
  if (!isset($entity->{$variables['field_name']}[$langcode]) || !is_array($entity->{$variables['field_name']}[$langcode])) {
    return;
  }
  $deltas = array_keys($entity->{$variables['field_name']}[$langcode]);

  foreach ($deltas as $delta) {
    $variables['parents'][$delta] = _references_index_fetch_ancestors($variables['field_name'], $variables['entity_type'], $variables['entity_id'], $langcode, $delta);
  }
}

/**
 * Theme the breadcrumbs.
 */
function theme_references_index_breadcrumbs($variables) {
  $html = array();
  $seperator = ' » ';
  foreach ($variables['parents'] as $delta => $parents) {
    if ($num = count($parents)) {
      $html[$delta] = '<div class="references_index_breadcrumb">';
      for ($i = 0; $i < $num; $i++) {
        $html[$delta] .= theme('references_index_breadcrumbs_item', $parents[$i]);
        $html[$delta] .= $seperator;
      }
      $html[$delta] = substr($html[$delta], 0, 0 - strlen($seperator));
      $html[$delta] .= '</div>';
    }
  }
  if (!count($html)) {
    return;
  }
  if (count($html) == 1) {
    return $html[$delta];
  }
  return '<ul class="references_index_breadcrumb_multi"><li>' . implode('</li><li>', $html) . '</li></ul>';
}

/**
 * Theme a breadcrumb item
 */
function theme_references_index_breadcrumbs_item($variables) {
  $entity_type = $variables['entity_type'];
  $entity_id = $variables['entity_id'];
  $entity = entity_load($entity_type, array($entity_id));
  $entity = $entity[$entity_id];
  $uri = entity_uri($entity_type, $entity);
  $info = entity_get_info($entity_type);
  list ($entity_id, $entity_vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $label = '';
  if ($info['bundles'][$bundle]['label']) {
    $label = t($info['bundles'][$bundle]['label']) . ': ';
  }
  return $label . l(entity_label($entity_type, $entity), $uri['path'], $uri['options']);
}

/**
 * Implements HOOK_preprocess_THEME().
 *
 * Preprocess theme function and fetch one level of childeren.
 */
function references_index_preprocess_references_index_tree(&$variables, $hook) {
  global $language;
  $variables['children'] = array();
  $entity = entity_load($variables['entity_type'], array($variables['entity_id']));
  $entity = $entity[$variables['entity_id']];
  $langcode = (isset($entity->{$variables['field_name']}[$language->language]) ? $language->language : LANGUAGE_NONE);
  if (!isset($entity->{$variables['field_name']})) {
    return;
  }
  $variables['language'] = $langcode;

  $deltas = array(0);
  if (isset($variables['field_name'][$langcode]) && isset($entity->{$variables['field_name'][$langcode]})) {
    $deltas = array_keys($entity->{$variables['field_name']}[$langcode]);
  }

  foreach ($deltas as $delta) {
    $variables['children'][$delta] = _references_index_fetch_children($variables['field_name'], $variables['entity_type'], $variables['entity_id'], $langcode, $delta, 1);
  }
}

/**
 * Theme a tree.
 */
function theme_references_index_tree($variables) {
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_library('system', 'jquery.cookie');
  drupal_add_library('system', 'ui.droppable');
  drupal_add_library('system', 'ui.draggable');
  drupal_add_library('system', 'ui.sortable');
  drupal_add_js(drupal_get_path('module', 'references_index') . '/js/jquery.ui.nestedSortable.js');
  drupal_add_js(drupal_get_path('module', 'references_index') . '/js/references_index.tree.js');
  drupal_add_css(drupal_get_path('module', 'references_index') . '/css/references_index.tree.css');

  $html = '';
  foreach ($variables['children'] as $delta => $children) {
    if (!count ($children)) {
      continue;
    }
    $variables['delta'] = $delta;
    $id = _references_index_create_tree_identifier($variables);
    $html .= '<div id="root_' . check_plain($id) . '" class="references-index-tree">';
    $html .= '<div class="ajax-status"><span class="insert-after-me"></span></div>';
    $child_vars = $variables; // avoid double reference.
    $child_vars['children'] = $children;
    $html .= theme('references_index_tree_children', $child_vars);
    $html .= '</div>';
  }
  return $html;
}

/**
 * Theme tree children
 */
function theme_references_index_tree_children($variables) {
  $id = _references_index_create_tree_identifier($variables);
  $html = '';
  $html .= '<ul id="children_' . $id . '">';
  foreach ($variables['children'] as $item) {
    $item['field_name'] = $variables['field_name'];
    $html .= theme('references_index_tree_item', $item);
  }
  $html .= '</ul>';
  return $html;
}

/**
 * Theme a tree item.
 */
function theme_references_index_tree_item($variables) {
  $item = $variables;
  $id = _references_index_create_tree_identifier($variables);
  $classes= array('tree-item');
  if (_references_index_number_of_childeren($item)) {
    $classes[] = 'has-children';
  }
  $html = '';
  $html .= '<li id="wrapper_' . $id . '" class="' . implode(' ', $classes) . '"><div id="item_' . $id . '" class="item">';
  $html .= theme('references_index_breadcrumbs_item', $item);
  $html .= '</div></li>';
  return $html;
}


