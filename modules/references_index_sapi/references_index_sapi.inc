<?php

/**
 * Search API data alteration callback that changes the order of index items.
 */
class SearchApiAlterReferencesIndexFilter extends SearchApiAbstractAlterCallback {

  public function alterItems(array &$items) {
    if (!$this->options['field']) {
      return;
    }
    $info = entity_get_info($this->index->item_type);
    $ids = array_keys($items);
    $field = field_info_field($this->options['field']);
    $table = _references_index_table_name($field['id']);

    $order = db_select($table, 'i')
      ->fields('i', array('entity_id', 'entity_id'))
      ->condition('entity_id', $ids, 'IN')
      ->orderBy('ileft')
      ->execute()
      ->fetchAllKeyed();
    $old_order = $items;
    $items = array();
    foreach($order as $key) {
      $items[$key] = $old_order[$key];
      unset($old_order[$key]);
    }
    $items += $old_order;
  }

  public function configurationForm() {
    $options = array(t('- None -'));
    $tables = variable_get('references_index_tables', array());
    $fields = array_keys($tables);
    foreach ($fields as $field) {
      // $info = field_info_field($field); Useless, no name
      $options[$field] = $field;
    }
    $form = array(
      'field' => array(
        '#type' => 'select',
        '#title' => t('Field'),
        '#default_value' => isset($this->options['field']) ? $this->options['field'] : array(),
        '#options' => $options,
        '#multiple' => FALSE,
      ),
    );
    return $form;
  }



}
