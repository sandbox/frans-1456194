/**
 * @file
 * Adds draggable tree.
 */




(function ($) {

  // helper to split the tree IDs
  String.prototype.normalizeTreeId  = function() {
    return this.substr(this.indexOf('_') + 1);
  };

  // Helper for the tree Cookie, stores open/close data.
  treeCookie = function(){
    this.cookieName = 'references_tree_index';
    this.options = { path: '/' };
    this.data = JSON.parse($.cookie(this.cookieName)) || {};
    this.save = function() {
      $.cookie(this.cookieName, JSON.stringify(this.data), this.options);
    };
    this.fetch = function(id) {
      if (typeof id == 'undefined') {
        return this.data;
      }
      if (typeof this.data[id] == 'undefined') {
        return null;
      }
      return this.data[id];
    };
    this.addOpen = function(id) {
      this.data[id] = 1;
      this.save();
    };
    this.addClose = function(id) {
      if (this.fetch(id)) {
        delete this.data[id];
        this.save();
      }
    };
    this.toggle = function(id) {
      if (this.fetch(id)) {
        this.addClose(id);
      }
      else {
        this.addOpen(id);
      }
    };
  };

  Drupal.behaviors.draggableReferencesIndexTree = {
    attach: function (context, settings) {
      $('.references-index-tree > ul', context).nestedSortable({
        placeholder: 'ui-state-highlight',
        listType : 'ul',
        disableNesting: 'no-nest',
        forcePlaceholderSize: true,
        handle: 'div',
        helper: 'clone',
        items: 'li',
        opacity: .6,
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        update: function(event, ui) {
          // No id means that the event is coming from something else.. we try to act.
          if (!$(ui.item).attr('id')) {
            if ($('a.create-new-node', ui.item).hasClass('create-new-node')) {
              var uri = $('a.create-new-node', ui.item).attr('href');
              var nodeId = ($(this).attr('id') || '').normalizeTreeId().split('__')[2];
              var mother = $(ui.item).parents('li.tree-item')[0];
              if (typeof mother != 'undefined') {
                // we zijn niet onder de root gesleept.
                // change the mother reference
                var motherId = ($(mother).attr('id') || '').normalizeTreeId().split('__')[2];
                uri = uri.replace('/' + nodeId, '/' + motherId);
              }
              var Order = ($(this).attr('id') || '').normalizeTreeId().split('__')[0] + '__order=' + $(ui.item).index();
              uri += (uri.indexOf('?') > 0 ? '&' : '?') + Order;
              $('a.create-new-node', ui.item).attr('href', uri);
              $('a.create-new-node', ui.item).trigger('click');
              var href = $('a.create-new-node', ui.item).attr('href');
              $(ui.item).remove(); // remove item, will show up on reload.
              window.location.href = href;
            }
            return true;
          }
          var elemid = '#' + $(ui.item).attr('id');
          var id = elemid.normalizeTreeId();
          var newOrder = $(elemid).index();
          var newBrotherUp = ($(elemid).prev('li').attr('id') || '').normalizeTreeId();
          var newBrotherDown = ($(elemid).next('li').attr('id') || '').normalizeTreeId();
          var newParent = ($(elemid).parents('li', '.references-index-tree').attr('id') || '').normalizeTreeId();
          if (newParent == '') {
            newParent = ($(elemid).parents('ul', '.references-index-tree').attr('id') || '').normalizeTreeId();
          }
          var ajaxelem = '.ajax-status .insert-after-me';
          var myajax = new Drupal.ajax(null, ajaxelem, {
            url: '/references_index/ajax/save/' + id,
            event: 'ajaxsave',
            keypress: false,
            prevent: false,
            progress: {
              type: 'throbber'
            }
          });
          myajax.options.data = {'parent': newParent, 'order': newOrder, 'sibling_up': newBrotherUp, 'sibling_down': newBrotherDown};
          myajax.eventResponse(ajaxelem, 'ajaxsave');
        },
        // On create (creating the tree)
        create: function (event, ui) {
          $('.references-index-tree li:has(ul)').addClass('branchopen');
          $('.references-index-tree li.has-children').addClass('no-nest').addClass('branchclosed');
        },
        start: function (event, ui) {
          // When items are coming in, add the classes that we need.
          if (!$(ui.item).hasClass('tree-item')) {
            $(ui.item).addClass('tree-item');
            $(ui.helper).addClass('tree-item');
            $('> div', ui.item).addClass('item');
            $('> div', ui.helper).addClass('item');
          }
        }
      });
      // Handle clicks in the tree.
      $('.references-index-tree li', context).live('click', function(e) {
        // pass through clicks on links.
        if (e.target.href != undefined) {
          return true;
        }
        // Ajax load children
        if ($(this).hasClass('has-children')) {
          var elem = $(this);
          $(elem).removeClass('has-children').removeClass('no-nest').addClass('branchopen').removeClass('branchclosed');
          var itemid = ($(elem).children('div.item').attr('id') || '').normalizeTreeId();
          var ajaxelem = '.ajax-status .insert-after-me';
          var myajax = new Drupal.ajax(null, ajaxelem, {
            url: '/references_index/ajax/load/' + itemid,
            event: 'ajaxload',
            keypress: false,
            prevent: false,
            progress: {
              type: 'throbber'
            }
          });
          myajax.eventResponse(ajaxelem, 'ajaxload');
          (new treeCookie).addOpen(itemid);
          return false;
        }
        // toggle open/close.
        if ($(this).children('ul').length) {
          var itemid = ($(this).attr('id') || '').normalizeTreeId();
          (new treeCookie).toggle(itemid);
          $(this).toggleClass('branchopen').toggleClass('branchclosed');
          $('.references-index-tree li').removeClass('no-nest');
          $('.references-index-tree li.branchclosed').addClass('no-nest');
          $('.references-index-tree li.branchclosed').find('li').addClass('no-nest');
        }
        return false;
      });

      $('.references-index-tree', context).disableSelection();

      // click on items in the cookies
      var c = (new treeCookie).fetch();
      for(item in c) {
        var selector = '#wrapper_' + item;
        $(selector + '.has-children', context).trigger('click');
      }
    }
  }

})(jQuery);